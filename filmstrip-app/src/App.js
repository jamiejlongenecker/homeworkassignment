import React, { useState, useEffect } from 'react';
import ImageMain from './components/ImageMain';
import ImageThumbnail from './components/ImageThumbnail';
import './App.css';

const App = () => {
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [imageDisplayed, setImageDisplayed] = useState([]);
  const [gallerySize, setGallerySize] = useState(4);
  const [error, setError] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  const getData = async() => {
    let response = await fetch('/api')
      .then((res) => res.json())
      .then((result) => {
          console.log('result: ', result);
          setIsLoaded(true);
          setData(result);
          setImageDisplayed(result.slice(0, gallerySize));
      },
      (error) => {
        setIsLoaded(true);
        setError(error);
      });

      return response;
  }

  useEffect(() => {
    getData();
  }, []);

  const onClickPrevious = () => {
    if (count > 0) {
      let addImage = 
        data[data.indexOf(imageDisplayed[0]) -1];
      if (addImage) {
        imageDisplayed.pop();
        imageDisplayed.unshift(addImage);
      }

      setCount(count - 1);
      setImageDisplayed(imageDisplayed);
    }
  };

  const onClickNext = () => {
    if (count < data.length - 1) {
      let addImage = 
        data[data.indexOf(imageDisplayed[imageDisplayed.length - 1]) + 1];
      if (addImage) {
        imageDisplayed.shift();
        imageDisplayed.push(addImage);
      }
      setCount(count + 1);
      setImageDisplayed(imageDisplayed);
    }
  };

  const handleThumbnailClick = (imageId) => {
    setCount(data.findIndex((i) => i.id === imageId));
  };

  if(error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div> Loading...</div>;
  } else {
    return (
      <div>
        <header>Website Templates</header>
        <div className="container">
          <div className="targetImageStyle">
          {data.length > 0 && (
            <ImageMain
              targetImage={data[count]}
            />
          )}
          </div>
        </div>
        <br />
        <br />
        <div className="container">
          <div >
            <span className="previous">
              {data.length > gallerySize &&
                count > 0 && (
                  <a>
                    <img
                      src="/previous.png"
                      alt="Pervious"
                      onClick={onClickPrevious}
                    />
                  </a>
                )}
            </span>
            <span className="next">
              {data.length > gallerySize &&
                count < data.length - 1 && (
                  <a>
                    <img
                      src="/next.png"
                      alt="Next"
                      onClick={onClickNext}
                    />
                  </a>
                )}
            </span>
            <br />
            <br />
            <div className="thumbnails">
            {data.length > 0 &&
              imageDisplayed.map((i) => (
                <span key={i.id}>
                  <ImageThumbnail
                    targetImage={i}
                    selected={data[count] === i}
                    onHandleClick={handleThumbnailClick}
                    alt="thumbnail"
                  />
                </span>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
