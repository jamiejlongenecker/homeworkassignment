import React, { Component } from 'react';
import './ImageMain.css';

class ImageMain extends Component {
    render() {
        const imagePath = '/large/'
        return (
            <div>
                <div>
                    <div className="row">
                        <div>
                            <img
                                src={imagePath + this.props.targetImage.image}
                                height='370'
                                width='430'
                                alt='Main Display' 
                            />
                        </div>
                        <div className="details">
                            {Object.entries(this.props.targetImage).map(([key, value]) => {
                                return (
                                    <div key={key}>
                                        <p>
                                        <span className="strong">
                                            <strong>{key}</strong>
                                        </span>
                                            &nbsp;&nbsp;{value}
                                        
                                        </p>
                                        <hr/>
                                    </div>
                                );
                            })}
                        
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default ImageMain;