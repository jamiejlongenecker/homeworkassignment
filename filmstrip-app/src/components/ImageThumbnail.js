import React, { Component } from 'react';

class ImageThumbnail extends Component {
    state = {
        hover: false,
    };

    onHover = () => {
        this.setState({ hover: !this.state.hover });
    };

    render() {
        const imagePath = '/thumbnails/';
        return (
            <a className={this.props.selected ? "active" : ""}>
                <img
                    src={imagePath + this.props.targetImage.thumbnail}
                    height='121'
                    width='145'
                    onMouseEnter={this.onHover}
                    onMouseLeave={this.onHover}
                    onClick={() => {
                        this.props.onHandleClick(this.props.targetImage.id)
                    }}
                    className={this.state.selectedImage === true ? "active" : ""}
                    alt='thumbnail'
                />
                <span className={this.props.selected ? "active" : ""}>
                    {this.props.targetImage.id}
                </span>
            </a>
        )
    }
};

export default ImageThumbnail;