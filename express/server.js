//  import necessary libraries
const express = require('express');
const cors = require('cors');
const PORT = process.env.PORT || 8080;
const data = require('./data/templates.json');
const app = express();

app.use(cors());

app.get('/', (req, res) => {
    res.send('good morning world & those who inhabit it');
});

app.get("/api", (req, res) => {
    res.send(data);
    // res.json({ message: "Hello from server!" });
});
  

app.listen(PORT, console.log(`Server started on port ${PORT}`));
